<?php

    /**
     * CSV to Unity JSON
     * A simple class to take the field values from a csv file and output
     * a JSON file ready to upload to Unity.
     *
     * Currently this requires that you set the fields needed for the object.
     * A future iteration would be to detect the field from a CSV
     * and create what is needed on the fly.
     *
     */
    class CsvToUnity
    {

        private $csvFilename;
        private $csvFileContents;
        private $csvPhpArray = [];

        public function __construct($csvFile)
        {
            if ($csvFile === "") {
                echo "Provide a file name";

                return;
            }

            $this->csvFilename = $csvFile;
            $this->getCsvFile();

        }

        function getCsvFile()
        {
            if (file_exists($this->csvFilename) && ($this->csvFileContents = fopen($this->csvFilename,
                    "r")) !== false) {
                $this->buildPhpArray();
            }
        }

        function buildPhpArray()
        {
            $row       = 0;
            $keysArray = [];

            while (($data = fgetcsv($this->csvFileContents, 1000, ",")) !== false) {

                if ($row == 0) {

                    // Build an array of Keys
                    foreach ($data as $value) {
                        $keysArray[] = $value;
                    }

                } else {

                    //Build defaultPartnerInformation array
                    $defaultPartnerInformationArray = [];

                    for ($i = 0; $i < count($data); $i++) {

                        if ($i !== 0) {

                            $defaultPartnerInformationArray[] = [
                                "locationId" => null,
                                "keyField"   => $keysArray[$i],
                                "keyValue"   => $data[$i]
                            ];
                        }

                    }

                    $this->csvPhpArray[$data[0]] = [
                        "defaultPartnerInformation" => $defaultPartnerInformationArray
                    ];

                }

                $row++;

            }

            fclose($this->csvFileContents);

            $this->outputFinalJson();
        }

        function outputFinalJson()
        {
            $filenameArray = explode(".csv", $this->csvFilename);
            $jsonFile      = fopen($filenameArray[0] . '.json', "w");

            $jsonObj = json_encode($this->csvPhpArray);

            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    echo ' - No errors';
                    break;
                case JSON_ERROR_DEPTH:
                    echo ' - Maximum stack depth exceeded';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    echo ' - Underflow or the modes mismatch';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    echo ' - Unexpected control character found';
                    break;
                case JSON_ERROR_SYNTAX:
                    echo ' - Syntax error, malformed JSON';
                    break;
                case JSON_ERROR_UTF8:
                    echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                    break;
                default:
                    echo ' - Unknown error';
                    break;
            }

            fwrite($jsonFile, $jsonObj);
        }

    }

    // Update the file here
    $convert = new CsvToUnity('csv-files/ogca-trackers.csv');
