# CSV to Unity JSON Object

A simple class to take the field values from a csv file and output a JSON file ready to upload to Unity.

Currently this requires that you set the fields needed for the object.
A future iteration would be to detect the field from a CSV and create what is needed on the fly. 

Its a quick implementation, so it is still open to suggestions for improvement.

## The Setup
1. Create your CSV file:
1. Copy the csv file to `csv-files/` folder
2. Open the index.php file and replace `$convert = new CsvToUnity({path-to-file})`
3. Edit the `buildPhpArray()` method to fit your csv fields

The Php function that reads the csv file. The first column title should be: `partnerId`.
The following columns must the be the key name that will be use inside unity.

csv file

| partnerId | sitename | sitecode |
|:------|------|-------|
| 00 | Ruby Fortune | ruby-fortune |


## To run
1. In the command line in the directory run `php index.php`

## Wishlist
- [ ] Add a GUI to allow input of CSV file
- [ ] Add option for choosing save destination of JSON file
- [x] Auto detect fields from csv for which to build objects